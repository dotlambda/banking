import json
import openpyxl

data = {}
data['databases'] = []

BLZ = 1
BIC = 2
INSTITUTE = 3
ORGANIZATION = 6
URL = 24

# Open official list
wb = openpyxl.load_workbook("fints_institute.xlsx", data_only=True)
worksheet = wb["fints_institute_Master"]
for row in worksheet.iter_rows(min_row=2):
    if row[BLZ].value:
        data['databases'].append({
            'blz': row[BLZ].value,
            'bic': str(row[BIC].value).lower(),
            'institute': row[INSTITUTE].value,
            'organization': row[ORGANIZATION].value.lower() if row[ORGANIZATION].value else "",
            'url': row[URL].value
        })

# Add custom entries
data['databases'].append({
        'blz': "20041177",
        'bic': '',
        'institute': 'comdirect bank AG',
        'organization': 'BdB',
        'url': 'https://fints.comdirect.de/fints'
    })

# Dummy data for testing purpose
data['databases'].append({
        'blz': "00000000",
        'bic': '',
        'institute': 'Test Bank',
        'organization': 'bdb',
        'url': 'http://0.0.0.0'
    })

with open('resources/database.json', 'w') as outfile:
    json.dump(data, outfile)
