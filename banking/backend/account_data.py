# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
from gi.repository import GObject

from banking.backend.transaction_data import TransactionData


class AccountData(GObject.GObject):
    def __init__(self, account):
        super().__init__()
        self._account = account

        self._transactions = []
        if self._account['transactions']:
            for transaction in self._account['transactions']:
                data = TransactionData(transaction)
                self._transactions.append(data)

    @property
    def bank_name(self) -> str:
        return self._account['bank_name']

    @property
    def iban(self) -> str:
        return self._account['iban'] or ''

    @property
    def association(self) -> str:
        return self._account['association']

    @property
    def balance(self) -> float:
        return float(self._account['balance'])

    @property
    def currency(self) -> str:
        return self._account['currency']

    @property
    def product_name(self) -> str:
        return self._account['product_name']

    @property
    def owner_name(self) -> str:
        return self._account['owner_name']

    @property
    def transactions(self) -> list:
        return self._transactions

    @property
    def last_updated(self) -> str:
        timestamp = self._account['last_updated']
        return datetime.utcfromtimestamp(timestamp).strftime('%a, %-d. %B')
