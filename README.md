# Banking

Banking application for small screens

## Introduction
This is a FinTS banking application using python fints as backend. This application
is able to connect to your bank and will show your current balance and transactions.
It has been created for my upcoming Librem 5 smartphone by Purism.

## Screenshots

![screenshot](data/screenshots/banking2.png)

## Contributing
Want to help make things better?
Take a look at the [Contributing guide](./CONTRIBUTING.md) for an introduction.

# Development

## Test Server
- https://github.com/thomet/hbci4javaserver
- http://subsembly.com/fints-api.html
- http://hbci4java.kapott.org/demoserver.html
