# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# pylint: disable=C0413

import datetime
from gettext import gettext as _
import json
import logging
import os
from pathlib import Path
from schwifty import IBAN
import threading
import time
import mt940
from fints.client import FinTSOperations, FinTS3PinTanClient, NeedTANResponse
from fints.exceptions import FinTSClientPINError
from fints.formals import CreditDebit2
from fints.models import SEPAAccount
from gi.repository import Adw, Gtk, Gio, GObject, GLib
from pysqlitecipher import sqlitewrapper

from banking.backend.account_data import AccountData
import banking.backend.fints_extra
import banking.config_manager as config
from banking.widgets.mechanism_dialog import MechanismDialog
from banking.widgets.tan_dialog import TanDialog
from banking.backend.transaction_data import TransactionData


BASE_KEY = "org.tabos.banking"

TABLE_CLIENTS = 'clients'
TABLE_ACCOUNTS = 'accounts'
TABLE_TRANSACTIONS = 'transactions'
TABLE_CATEGORIES = 'categories'

CLIENT_COLUMNS = [
    ["user", "TEXT"],
    ["password", "TEXT"],
    ["server", "TEXT"],
    ["blz", "TEXT"],
    ["tan-mechanism", "TEXT"],
]

ACCOUNT_COLUMNS = [
    ["client_id", "INT"],
    ["association", "TEXT"],
    ["bank_name", "TEXT"],
    ["product_name", "TEXT"],
    ["currency", "TEXT"],
    ["iban", "TEXT"],
    ["subaccount_number", "TEXT"],
    ["owner_name", "TEXT"],
    ["last_updated", "REAL"],
    ["balance", "REAL"]
]

TRANSACTION_COLUMNS = [
    ["account_id", "INT"],
    ["status", "TEXT"],
    ["funds_code", "TEXT"],
    ["amount", "REAL"],
    ["id", "TEXT"],
    ["customer_reference", "TEXT"],
    ["bank_reference", "TEXT"],
    ["extra_details", "TEXT"],
    ["currency", "TEXT"],
    ["date", "TEXT"],
    ["entry_date", "TEXT"],
    ["guessed_entry_date", "TEXT"],
    ["transaction_code", "TEXT"],
    ["posting_text", "TEXT"],
    ["prima_nota", "TEXT"],
    ["purpose", "TEXT"],
    ["applicant_bin", "TEXT"],
    ["applicant_iban", "TEXT"],
    ["applicant_name", "TEXT"],
    ["return_debit_notes", "TEXT"],
    ["recipient_name", "TEXT"],
    ["additional_purpose", "TEXT"],
    ['additional_position_reference', 'TEXT'],
    ['end_to_end_reference', 'TEXT']
]

CATEGORIES_COLUMNS = [
    ["account_id", "INT"],
    ["name", "TEXT"],
    ["category", "TEXT"]
]


class BankingBackend(GObject.Object):
    CATEGORY_DB = {
        "salary": {"name": _("Salary"), "icon": "bank-symbolic"},
        "hotel": {"name": _("Hotel"), "icon": "bed-symbolic"},
        "streaming": {"name": _("Streaming"), "icon": "camera-symbolic"},
        "atm": {"name": _("ATM"), "icon": "credit-card-symbolic"},
        "driving": {"name": _("Car"), "icon": "driving-symbolic"},
        "fast-food": {"name": _("Fast Food"), "icon": "fast-food-symbolic"},
        "fuel": {"name": _("Fuel"), "icon": "fuel-symbolic"},
        "games": {"name": _("Games"), "icon": "gamepad-symbolic"},
        "stock": {"name": _("Stock"), "icon": "money-symbolic"},
        "music": {"name": _("Music"), "icon": "music-note-symbolic"},
        "animal": {"name": _("Animal"), "icon": "penguin-symbolic"},
        "technology": {"name": _("Technology"), "icon": "phonelink2-symbolic"},
        "pharmarcy": {"name": _("Pharmarcy"), "icon": "plus-symbolic"},
        "restaurant": {"name": _("Restaurant"), "icon": "restaurant-symbolic"},
        "school": {"name": _("School"), "icon": "school-symbolic"},
        "shopping": {"name": _("Shopping"), "icon": "shopping-cart-symbolic"},
        "smartphone": {"name": _("Phone"), "icon": "smartphone-symbolic"},
        "leisure": {"name": _("Leisure"), "icon": "sun-symbolic"},
        "television": {"name": _("Television"), "icon": "tv-symbolic"},
        "fitness": {"name": _("Fitness"), "icon": "weight2-symbolic"},
        "home": {"name": _("Home"), "icon": "go-home-symbolic"},
        "barber": {"name": _("Barber"), "icon": "edit-cut-symbolic"},
        "lottery": {"name": _("Lottery"), "icon": "money-symbolic"}
    }

    @GObject.Signal(flags=GObject.SignalFlags.RUN_FIRST)
    def accounts_refreshed(self):
        self._load_accounts()
        self.set_property('loading', False)

    @GObject.Signal(flags=GObject.SignalFlags.RUN_LAST, return_type=bool,
                    arg_types=(object, bool,),
                    accumulator=GObject.signal_accumulator_true_handled)
    def new_transaction(self, transaction: TransactionData, known_applicant: bool):
        pass

    def __init__(self):
        super().__init__()

        self._banking_path = os.path.join(GLib.get_user_data_dir(), "banking")
        if not os.path.exists(self._banking_path):
            os.makedirs(self._banking_path)

        logging.basicConfig(
            filename=os.path.join(self._banking_path, "banking.log"),
            level=logging.DEBUG)
        logging.info('Started')

        self._client = None
        self._settings = Gio.Settings.new(BASE_KEY)
        self._mechanisms = None
        self._tan_ret = None
        self._mechanism_ret = None
        self._tan_msg = None
        self.client_data = None
        self._refresh_thread = None

        self.database_path = self._banking_path + '/banking.sqlite'
        self.database = None
        # self.db_secure = False  # Testing only!
        self.db_secure = True
        self.check_same_thread = False
        self._database_loaded = False
        self.__loading = False

        gbytes = Gio.resources_lookup_data(
            '/org/tabos/banking/resources/database.json',
            Gio.ResourceLookupFlags.NONE)
        data = gbytes.get_data().decode('utf-8')
        self._client_db = json.loads(data)

        self._categories = {}

        self._accounts = []
        self._clients = []

    def _load_categories(self):
        self._categories = {}
        _, values = self.database.getDataFromTable(TABLE_CATEGORIES)

        for row in values:
            self._categories[row[2]] = (row[0], row[3])

    def category_set_avatar(self, avatar: Adw.Avatar, name: str) -> None:
        avatar.set_text("Banking")
        avatar.set_show_initials(False)

        try:
            cat = self.CATEGORY_DB[self._categories[name][1]]
            avatar.set_icon_name(cat['icon'])
        except (IndexError, KeyError):
            avatar.set_icon_name("dialog-question-symbolic")

    def category_set_avatar_by_category_name(self, avatar: Adw.Avatar, name: str) -> None:
        avatar.set_text("Banking")
        avatar.set_show_initials(False)

        try:
            cat = self.CATEGORY_DB[name]

            avatar.set_icon_name(cat['icon'])
        except (IndexError, KeyError):
            avatar.set_icon_name("dialog-question-symbolic")

    def category_get_name(self, name: str) -> str:
        try:
            cat = self.CATEGORY_DB[self._categories[name][1]]
            return cat['name']
        except (IndexError, KeyError):
            return _("Other Expenses")

    def category_get_internal_name(self, name: str) -> str:
        try:
            return self._categories[name][1]
        except (IndexError, KeyError):
            return ""

    def category_add_mapping(self, name: str, cat: str) -> None:
        if name in self._categories:
            category = self._categories[name][0]
            self.database.updateInTable(TABLE_CATEGORIES, category, 'category', cat, commit=True, raiseError=True)
        else:
            self.database.insertIntoTable(TABLE_CATEGORIES, [0, name, cat], commit=True)

        self._load_categories()

    def category_remove_mapping(self, name: str) -> None:
        if name in self._categories:
            category = self._categories[name][0]
            self.database.deleteDataInTable(TABLE_CATEGORIES, category, commit=True, raiseError=True)
            self._load_categories()

    @property
    def database_exists(self):
        return Path(self.database_path).is_file()

    @property
    def user(self):
        return self.client_data['user'] if self.client_data else ''

    @user.setter
    def user(self, user):
        self.client_data['user'] = user

    @property
    def blz(self):
        return self.client_data['blz']

    @blz.setter
    def blz(self, blz):
        self.client_data['blz'] = blz

    @property
    def server(self):
        return self.client_data['server'] if self.client_data else ''

    @server.setter
    def server(self, server):
        self.client_data['server'] = server

    @property
    def password(self):
        return self.client_data['password'] if self.client_data else ''

    @password.setter
    def password(self, password):
        self.client_data['password'] = password

    @property
    def safe_password(self):
        return ''

    @safe_password.setter
    def safe_password(self, safe_password):
        # Create database
        self.database = sqlitewrapper.SqliteCipher(self.database_path, self.check_same_thread, safe_password)

        # Add tables
        self.database.createTable(TABLE_CLIENTS, CLIENT_COLUMNS, makeSecure=self.db_secure, commit=True)
        self.database.createTable(TABLE_ACCOUNTS, ACCOUNT_COLUMNS, makeSecure=self.db_secure, commit=True)
        self.database.createTable(TABLE_TRANSACTIONS, TRANSACTION_COLUMNS, makeSecure=self.db_secure, commit=True)
        self.database.createTable(TABLE_CATEGORIES, CATEGORIES_COLUMNS, makeSecure=self.db_secure, commit=True)

    def is_safe_password_valid(self, safe_password):
        verifier = sqlitewrapper.SqliteCipher.getVerifier(self.database_path, self.check_same_thread)
        password_verifier = sqlitewrapper.SqliteCipher.sha512Convertor(safe_password)

        if verifier == password_verifier:
            if not self.database:
                self.database = sqlitewrapper.SqliteCipher(self.database_path, self.check_same_thread, safe_password)

            return True

        return False

    def load_database(self):
        if not self._database_loaded:
            self._load_categories()
            self._load_clients()
            self._load_accounts()

            self._database_loaded = True

    @property
    def accounts(self):
        return self._accounts

    @property
    def clients(self):
        return self._clients

    def find_institute(self, blz_or_bic):
        """
        Find institute based on user provided blz or bic information
        :param blz_or_bic: user provided blz or bic
        :return: database entry if found otherwise None
        """
        for db in self._client_db['databases']:
            blz = str(db['blz'])
            bic = str(db['bic'])
            if blz.lower() == blz_or_bic.lower() or bic.lower() == blz_or_bic.lower():
                return db

        return None

    def get_association(self, client_data):
        for row in self._client_db['databases']:
            blz = str(row['blz'])
            if blz == client_data['blz']:
                return row['organization']

        return None

    def _load_clients(self):
        self._clients = []

        clients_header, clients_val = self.database.getDataFromTable(TABLE_CLIENTS)

        for row in clients_val:
            client = dict(zip(clients_header, row))
            self._clients.append(client)

    def _load_accounts(self):
        self._accounts = []

        accounts_header, accounts_val = self.database.getDataFromTable(TABLE_ACCOUNTS)
        transaction_header, transaction_val = self.database.getDataFromTable(TABLE_TRANSACTIONS)

        for row in accounts_val:
            account = dict(zip(accounts_header, row))

            account_transactions = [dict(zip(transaction_header, x)) for x in transaction_val if x[1] == account['ID']]
            account['transactions'] = account_transactions
            account_data = AccountData(account)

            self._accounts.append(account_data)

    def _on_tan_response(self, _, retval, mainloop):
        self._tan_ret = retval
        mainloop.quit()

    def _ask_for_tan(self, response):
        """
        Ask user for a new TAN
        :param response: client response message
        """
        tan_dialog = TanDialog(response, use_header_bar=True)

        app = Gtk.Application.get_default()
        win = Gtk.Application.get_active_window(app)
        if win:
            tan_dialog.set_transient_for(win)

        # MainLoop is an ugly hack for the current API
        mainloop = GObject.MainLoop()
        tan_dialog.connect('response', self._on_tan_response, mainloop)
        tan_dialog.show()

        mainloop.run()

        if self._tan_ret == Gtk.ResponseType.APPLY:
            response = self._client.send_tan(response, tan_dialog.tan)
        else:
            response = None

        tan_dialog.destroy()

        return response

    def _on_mechanism_response(self, dialog, retval, client_data, event):
        if retval == Gtk.ResponseType.APPLY:
            mechanism = self.supported_mechanisms[dialog.mechanism][0]
            self.set_tan_mechanism(client_data, mechanism)
            self._client.set_tan_mechanism(mechanism)

        dialog.destroy()
        event.set()

    def get_mechanism(self, client_data, event):
        self.supported_mechanisms = list(self._client.get_tan_mechanisms().items())

        mechanism_dialog = MechanismDialog(self.supported_mechanisms,
                                           use_header_bar=True)
        app = Gtk.Application.get_default()
        win = Gtk.Application.get_active_window(app)
        if win:
            mechanism_dialog.set_modal(True)
            mechanism_dialog.set_transient_for(win)

        mechanism_dialog.connect('response', self._on_mechanism_response, client_data, event)
        mechanism_dialog.show()

    def connect_client(self, client_data):
        if self._client or client_data['blz'] == '00000000':
            return True

        logging.debug('Connecting to server...')
        self._client = FinTS3PinTanClient(
            client_data['blz'],
            client_data['user'],
            client_data['password'],
            client_data['server'],
            product_id='AA3B821AAECEA62FB87C27EF3')

        if self._client.get_current_tan_mechanism() is None:
            self._client.fetch_tan_mechanisms()

            if client_data['tan-mechanism'] != '':
                self._client.set_tan_mechanism(client_data['tan-mechanism'])
            else:
                event = threading.Event()
                GLib.idle_add(self.get_mechanism, client_data, event)
                event.wait()

        if self._client.init_tan_response:
            self._ask_for_tan(self._client.init_tan_response)

        return True

    def _get_credit_card_balance(self, sepa_access, credit_card_number):
        credit_card_transactions = self._client.get_credit_card_transactions(
            sepa_access,
            credit_card_number,
            datetime.date.today() - datetime.timedelta(days=1),
            datetime.date.today())

        if not (credit_card_transactions
                and isinstance(
                    credit_card_transactions[0], banking.backend.fints_extra.DIKKU2)):
            return None

        # Return fints.formals.Balance1 format
        balance1 = credit_card_transactions[0].balance
        return balance1

    def _get_credit_card_transactions(self, sepa_access, credit_card_number):
        credit_card_transactions = self._client.get_credit_card_transactions(
            sepa_access,
            credit_card_number,
            datetime.date.today() - datetime.timedelta(days=config.get_safe_days()),
            datetime.date.today())

        if not (credit_card_transactions
                and isinstance(
                    credit_card_transactions[0],
                    banking.backend.fints_extra.DIKKU2)
                and hasattr(credit_card_transactions[0], 'transactions')):
            # Actually these are at least 3 different cases
            # If only the last condition is false, and there is
            # no 'transactions' field it only means that there were simply
            # no transactions in the give timeframe so retuning the empty list
            # is completely valid, but if we didn't get back
            # a banking.fints_extra.DIKKU2 type
            # then something very unexpected must have happened like a warning:
            # 'FinTSParserWarning: Ignoring parser error and
            #  returning generic object'
            # Or some completely unexpected type was returned and parsed in.
            return []

        # List of CreditCardTransaction1 objects
        transactions = credit_card_transactions[0].transactions

        # CreditCardTransaction1 type objects mapped to mt940 encodable dicts
        mt940_encodable_transactions = [{
            'iban': '',
            'date': str(t.receipt_date),
            'entry_date': str(t.booking_date),
            'guessed_entry_date': str(t.value_date),
            'currency': t.currency,
            'posting_text': '',
            'amount': {
                'amount': (
                    ('-' if t.credit_debit == CreditDebit2.DEBIT else '')
                    + str(t.booked_amount)),
                'currency': t.booked_currency
            },
            'applicant_name': '',
            'applicant_iban': '',
            'applicant_bin': '',
            'purpose': ' '.join([x for x in t.memo if x]),
            'applicant_creditor_id': '',
            'additional_position_reference': '',
            'end_to_end_reference': t.booking_reference

        } for t in transactions]

        return mt940_encodable_transactions

    def _init_dummy_bank(self):
        account_info = {}
        account_info['association'] = 'dsgv'
        account_info['bank_name'] = 'Test Bank'
        account_info['product_name'] = 'Giro'
        account_info['currency'] = 'EUR'
        account_info['last_updated'] = time.time()
        account_info['iban'] = '340027267'
        account_info['subaccount_number'] = ''
        account_info['owner_name'] = "Max Mustermann"
        account_info['balance'] = 38013.54

        account_id = self.add_account(self.client_data, account_info)
        transaction_json = []

        transaction = {}
        transaction["status"] = 'C'
        transaction['funds_code'] = ''
        transaction["amount"] = -920.00
        transaction["id"] = ''
        transaction["customer_reference"] = ''
        transaction["bank_reference"] = ''
        transaction["extra_details"] = ''
        transaction["currency"] = 'EUR'
        transaction["date"] = '2013-01-02'
        transaction["entry_date"] = '2013-01-02'
        transaction["guessed_entry_date"] = '2013-01-02'
        transaction["transaction_code"] = ''
        transaction["posting_text"] = 'MIETE JANUAR 2013'
        transaction["prima_nota"] = ''
        transaction["purpose"] = ''
        transaction["applicant_bin"] = ''
        transaction["applicant_iban"] = ''
        transaction["applicant_name"] = 'Hausverwaltung Musterweg 2'
        transaction["return_debit_notes"] = ''
        transaction["recipient_name"] = ''
        transaction["additional_purpose"] = ''
        transaction["additional_position_reference"] = ''
        transaction["end_to_end_reference"] = ''
        transaction_json.append(transaction)

        transaction = {}
        transaction["status"] = 'D'
        transaction['funds_code'] = ''
        transaction["amount"] = 2420.37
        transaction["id"] = ''
        transaction["customer_reference"] = ''
        transaction["bank_reference"] = ''
        transaction["extra_details"] = ''
        transaction["currency"] = 'EUR'
        transaction["date"] = '2012-12-27'
        transaction["entry_date"] = '2012-12-27'
        transaction["guessed_entry_date"] = '2012-12-27'
        transaction["transaction_code"] = ''
        transaction["posting_text"] = 'GEHALT 01 2013'
        transaction["prima_nota"] = ''
        transaction["purpose"] = ''
        transaction["applicant_bin"] = ''
        transaction["applicant_iban"] = ''
        transaction["applicant_name"] = 'Max Mustermann GmbH'
        transaction["return_debit_notes"] = ''
        transaction["recipient_name"] = ''
        transaction["additional_purpose"] = ''
        transaction["additional_position_reference"] = ''
        transaction["end_to_end_reference"] = ''
        transaction_json.append(transaction)

        transaction = {}
        transaction["status"] = 'C'
        transaction['funds_code'] = ''
        transaction["amount"] = -9.79
        transaction["id"] = ''
        transaction["customer_reference"] = ''
        transaction["bank_reference"] = ''
        transaction["extra_details"] = ''
        transaction["currency"] = 'EUR'
        transaction["date"] = '2012-12-26'
        transaction["entry_date"] = '2012-12-26'
        transaction["guessed_entry_date"] = '2012-12-26'
        transaction["transaction_code"] = ''
        transaction["posting_text"] = 'SIEHE ANLAGE'
        transaction["prima_nota"] = ''
        transaction["purpose"] = ''
        transaction["applicant_bin"] = ''
        transaction["applicant_iban"] = ''
        transaction["applicant_name"] = 'Entgeltabrechnung'
        transaction["return_debit_notes"] = ''
        transaction["recipient_name"] = ''
        transaction["additional_purpose"] = ''
        transaction["additional_position_reference"] = ''
        transaction["end_to_end_reference"] = ''
        transaction_json.append(transaction)

        transaction = {}
        transaction["status"] = 'C'
        transaction['funds_code'] = ''
        transaction["amount"] = -123.00
        transaction["id"] = ''
        transaction["customer_reference"] = ''
        transaction["bank_reference"] = ''
        transaction["extra_details"] = ''
        transaction["currency"] = 'EUR'
        transaction["date"] = '2012-12-23'
        transaction["entry_date"] = '2012-12-23'
        transaction["guessed_entry_date"] = '2012-12-23'
        transaction["transaction_code"] = ''
        transaction["posting_text"] = 'ELV466776534678'
        transaction["prima_nota"] = ''
        transaction["purpose"] = ''
        transaction["applicant_bin"] = ''
        transaction["applicant_iban"] = ''
        transaction["applicant_name"] = 'KFZ Versicherung GmbH'
        transaction["return_debit_notes"] = ''
        transaction["recipient_name"] = ''
        transaction["additional_purpose"] = ''
        transaction["additional_position_reference"] = ''
        transaction["end_to_end_reference"] = ''
        transaction_json.append(transaction)

        transaction = {}
        transaction["status"] = 'D'
        transaction['funds_code'] = ''
        transaction["amount"] = 216.15
        transaction["id"] = ''
        transaction["customer_reference"] = ''
        transaction["bank_reference"] = ''
        transaction["extra_details"] = ''
        transaction["currency"] = 'EUR'
        transaction["date"] = '2012-12-21'
        transaction["entry_date"] = '2012-12-21'
        transaction["guessed_entry_date"] = '2012-12-21'
        transaction["transaction_code"] = ''
        transaction["posting_text"] = 'RUECKERSTATTUNG KD.-NR. 123456'
        transaction["prima_nota"] = ''
        transaction["purpose"] = ''
        transaction["applicant_bin"] = ''
        transaction["applicant_iban"] = ''
        transaction["applicant_name"] = 'Vertragkuendigung'
        transaction["return_debit_notes"] = ''
        transaction["recipient_name"] = ''
        transaction["additional_purpose"] = ''
        transaction["additional_position_reference"] = ''
        transaction["end_to_end_reference"] = ''
        transaction_json.append(transaction)

        transaction = {}
        transaction["status"] = 'C'
        transaction['funds_code'] = ''
        transaction["amount"] = -75.00
        transaction["id"] = ''
        transaction["customer_reference"] = ''
        transaction["bank_reference"] = ''
        transaction["extra_details"] = ''
        transaction["currency"] = 'EUR'
        transaction["date"] = '2012-12-21'
        transaction["entry_date"] = '2012-12-21'
        transaction["guessed_entry_date"] = '2012-12-21'
        transaction["transaction_code"] = ''
        transaction["posting_text"] = 'AUTOMAT 12345678 LINDENALLEE'
        transaction["prima_nota"] = ''
        transaction["purpose"] = ''
        transaction["applicant_bin"] = ''
        transaction["applicant_iban"] = ''
        transaction["applicant_name"] = 'Barauszahlung'
        transaction["return_debit_notes"] = ''
        transaction["recipient_name"] = ''
        transaction["additional_purpose"] = ''
        transaction["additional_position_reference"] = ''
        transaction["end_to_end_reference"] = ''
        transaction_json.append(transaction)

        if account_id >= 0:
            self.add_transactions(account_id, transaction_json)

        account = {}
        account['transactions'] = transaction_json
        account_data = AccountData(account)
        self._accounts.append(account_data)

        account_info = {}
        account_info['association'] = 'bdb'
        account_info['bank_name'] = 'Test Bank'
        account_info['product_name'] = 'KapitalKonto'
        account_info['currency'] = 'EUR'
        account_info['last_updated'] = time.time()
        account_info['iban'] = 'DE111234567800000002'
        account_info['subaccount_number'] = ''
        account_info['owner_name'] = "Jane Doe"
        account_info['balance'] = 321.00

        account_id = self.add_account(self.client_data, account_info)

        transaction_json = []
        if account_id >= 0:
            self.add_transactions(account_id, transaction_json)

        account = {}
        account['transactions'] = transaction_json
        account_data = AccountData(account)
        self._accounts.append(account_data)

    def _get_sepa_account(self, iban):
        return next((a for a in self.sepa_accounts if a.iban == iban), None)

    def _request_account_data(self):
        if self.client_data['blz'] == "00000000":
            print("Special dummy bank")
            self._init_dummy_bank()
            self.emit('accounts_refreshed')
            return

        try:
            with self._client:
                info = self._client.get_information()
                if not info:
                    logging.warning("Could not access client information, aborting.")
                    return

                if self._client.init_tan_response:
                    self._ask_for_tan(self._client.init_tan_response)

                logging.debug('Got generic bank information')
                if 'accounts' not in info:
                    logging.warning("No accounts found within client information, aborting.")
                    return

                logging.debug('Num accounts: %d',
                              len(info['accounts']))

                if info['bank']['supported_operations'][
                        FinTSOperations.GET_SEPA_ACCOUNTS]:
                    self.sepa_accounts = self._client.get_sepa_accounts()
                else:
                    self.sepa_accounts = []

                for account in info['accounts']:
                    # Account info
                    account_info = {}
                    account_info['association'] = self.get_association(self.client_data)
                    account_info['bank_name'] = info['bank']['name']
                    account_info['product_name'] = account.get('product_name', '')
                    account_info['currency'] = account['currency']
                    account_info['last_updated'] = time.time()
                    account_info['iban'] = account.get('iban', '')
                    account_info['subaccount_number'] = account.get('subaccount_number', '')
                    account_info['owner_name'] = ', '.join(account['owner_name'])

                    credit_card_number = None

                    logging.debug('Get balance for: %s',
                                  account_info['product_name'])

                    # Get balance
                    sepa_account = self._get_sepa_account(account_info['iban'])
                    if sepa_account:
                        logging.debug("SEPA Account found")
                    else:
                        logging.debug("SEPA Account not found, checking for credit card information...")

                        gcct_supported = account['supported_operations'][
                            FinTSOperations.GET_CREDIT_CARD_TRANSACTIONS]

                        if not (account['subaccount_number'] and gcct_supported):
                            logging.debug('No SEPA access found, returning')
                            continue

                        # get_credit_card_transactions will need an
                        # 'accountnumber' field,
                        # but certain subaccounts only have an
                        # 'account_number' field, so let's wrap this
                        # into a SEPAAccount.
                        sepa_account = SEPAAccount(
                            iban='',
                            # The bic is a must have, so let's assume that
                            # there is at least 1 sepa_account
                            # Otherwise we should use db['bic']
                            bic=self.sepa_accounts[0].bic,
                            accountnumber=account['account_number'],
                            subaccount=account['subaccount_number'],
                            blz=self.blz)

                        credit_card_number = account['account_number']
                        logging.debug("Assuming credit card")

                    if account['supported_operations'][
                            FinTSOperations.GET_BALANCE]:
                        balance = self._client.get_balance(sepa_account)
                        while isinstance(balance, NeedTANResponse):
                            balance = self._ask_for_tan(balance)
                        account_info['balance'] = float(balance.amount.amount)
                    elif account['supported_operations'][
                            FinTSOperations.GET_CREDIT_CARD_TRANSACTIONS]:
                        balance = self._get_credit_card_balance(
                            sepa_account, credit_card_number)
                        account_info['balance'] = float(balance.amount)
                    else:
                        logging.warning("Could not read balance, using 0.00 for balance.")
                        account_info['balance'] = float(0.00)

                    # Get transactions
                    logging.debug('Get transactions')
                    if (credit_card_number
                            and account['supported_operations'][
                                FinTSOperations.GET_CREDIT_CARD_TRANSACTIONS]):
                        transactions = self._get_credit_card_transactions(
                            sepa_account,
                            credit_card_number)
                    elif account['supported_operations'][
                            FinTSOperations.GET_TRANSACTIONS]:
                        transactions = self._client.get_transactions(
                            sepa_account,
                            datetime.date.today()
                            - datetime.timedelta(days=config.get_safe_days()),
                            datetime.date.today())
                    else:
                        logging.warning("Could not read transaction, using empty list for transactions.")
                        transactions = []

                    while isinstance(transactions, NeedTANResponse):
                        transactions = self._ask_for_tan(transactions)

                    dump = json.dumps(transactions, indent=4, cls=mt940.JSONEncoder).replace('null', '""')
                    transaction_json = json.loads(dump)
                    for item in transaction_json:
                        item['amount'] = float(item['amount']['amount'])

                    account_id = self.add_account(self.client_data, account_info)
                    if account_id >= 0:
                        self.add_transactions(account_id, transaction_json)

                    account['transactions'] = transaction_json
                    account_data = AccountData(account)
                    self._accounts.append(account_data)

        except FinTSClientPINError as err:
            app = Gtk.Application.get_default()
            win = Gtk.Application.get_active_window(app)
            dialog = Gtk.MessageDialog(
                transient_for=win,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.CANCEL,
                text=err
            )
            dialog.set_modal(True)
            dialog.connect("response", self._on_error_dialog)
            dialog.show()

        self.emit('accounts_refreshed')

    def _on_error_dialog(self, window, _):
        Gtk.Window.destroy(window)

    def _refresh_accounts_thread(self):
        if not self._client:
            self.client_data = self.get_client()
            ret = self.connect_client(self.client_data)
            if not ret:
                return

        self._request_account_data()

    def refresh_accounts(self):
        if self._refresh_thread and self._refresh_thread.is_alive():
            return

        self.set_property('loading', True)
        self._refresh_thread = threading.Thread(target=self._refresh_accounts_thread)
        self._refresh_thread.daemon = True
        self._refresh_thread.start()

    def add_client(self, user: str, password: str, server: str, blz: str) -> None:
        _, values = self.database.getDataFromTable(TABLE_CLIENTS, omitID=True)

        client_data = [
            user,
            password,
            server,
            blz,
            ''
        ]

        if client_data in values:
            logging.info('add_client: Found existing client, exit')
            return

        logging.info('add_client: Adding new client')
        self.database.insertIntoTable(TABLE_CLIENTS, client_data, commit=True)

    def get_client(self):
        header, values = self.database.getDataFromTable(TABLE_CLIENTS)

        # TODO: Only one client support at the moment
        ret = dict(zip(header, values[0]))

        return ret

    def set_tan_mechanism(self, client_data, value):
        self.database.updateInTable(TABLE_CLIENTS, client_data['ID'], 'tan-mechanism', value, commit=True, raiseError=True)

    def add_account(self, client_data, account_info):
        account_data = [
            client_data['ID'],
            account_info["association"],
            account_info["bank_name"],
            account_info["product_name"],
            account_info["currency"],
            account_info["iban"],
            account_info["subaccount_number"],
            account_info["owner_name"],
            account_info["last_updated"],
            account_info["balance"]
        ]

        _, values = self.database.getDataFromTable(TABLE_ACCOUNTS)

        for row in values:
            if account_data[0:8] == row[1:9]:
                logging.info("add_account: Account already exists, updating")
                account_id = row[0]
                self.database.updateInTable(TABLE_ACCOUNTS, account_id, "last_updated", account_info["last_updated"], commit=True, raiseError=True)
                self.database.updateInTable(TABLE_ACCOUNTS, account_id, "balance", account_info["balance"], commit=True, raiseError=True)
                return account_id

        logging.info("add_account: Adding new account")
        self.database.insertIntoTable(TABLE_ACCOUNTS, account_data, commit=True)

        # FIXME: Stop guessing
        return len(values)

    def add_transactions(self, account_id, transactions):
        transaction_header, values = self.database.getDataFromTable(TABLE_TRANSACTIONS, omitID=True)

        existing_list = account_id in (list[0] for list in values)

        for transaction in transactions:
            transaction_data = [
                account_id,
                transaction.get("status") or '',
                transaction.get("funds_code") or '',
                transaction["amount"],
                transaction.get("id") or '',
                transaction.get("customer_reference") or '',
                transaction.get("bank_reference") or '',
                transaction.get("extra_details") or '',
                transaction["currency"],
                transaction["date"],
                transaction["entry_date"],
                transaction["guessed_entry_date"],
                transaction.get("transaction_code") or '',
                transaction["posting_text"],
                transaction.get("prima_nota") or '',
                transaction["purpose"],
                transaction["applicant_bin"],
                transaction["applicant_iban"],
                transaction["applicant_name"],
                transaction.get("return_debit_notes") or '',
                transaction.get("recipient_name") or '',
                transaction.get("additional_purpose") or '',
                transaction.get("additional_position_reference") or '',
                transaction.get("end_to_end_reference") or ''
            ]

            if transaction_data in values:
                continue

            self.database.insertIntoTable(TABLE_TRANSACTIONS, transaction_data, commit=True)

            # Only show notification for new entries to an existing database,
            # otherwise we would spam the user during initial setup
            if existing_list:
                data = TransactionData(transaction)

                known_applicant = False
                for row in values:
                    existing_transaction = dict(zip(transaction_header, row))

                    if existing_transaction['applicant_iban'] == transaction['applicant_iban']:
                        known_applicant = True
                        break

                self.emit('new-transaction', data, known_applicant)

    @GObject.Property(type=bool, default=False)
    def loading(self):
        return self.__loading

    @loading.setter
    def loading(self, value):
        self.__loading = value

    def sepa_transfer(self,
                      account: AccountData,
                      account_name: str,
                      recipient_name: str,
                      iban: IBAN,
                      amount: float,
                      reason: str):

        if not self._client:
            self.client_data = self.get_client()
            self.connect_client(self.client_data)

        res = self._client.simple_sepa_transfer(account=account,
                                                iban=str(iban),
                                                bic=iban.bank_code,
                                                amount=amount,
                                                recipient_name=recipient_name,
                                                account_name=account_name,
                                                reason=reason,
                                                endtoend_id='NOTPROVIDED',)
        if isinstance(res, NeedTANResponse):
            self._ask_for_tan(res)
