# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import locale
from gi.repository import Gtk

from banking.backend.account_data import AccountData
from banking.widgets.transaction_row import TransactionRow


@Gtk.Template(resource_path='/org/tabos/banking/ui/account_row.ui')
class AccountRow(Gtk.ListBoxRow):
    __gtype_name__ = 'AccountRow'

    _image = Gtk.Template.Child()
    _product_label = Gtk.Template.Child()
    _account_label = Gtk.Template.Child()
    _owner_label = Gtk.Template.Child()
    _balance_label = Gtk.Template.Child()

    def __init__(self, account, backend):
        super().__init__()

        self.transactions = []
        self._backend = backend
        self.set_info(account)

    def set_info(self, account):
        self._account = account

        # Set image
        if 'comdirect bank' in self._account.bank_name.lower():
            self._image.set_from_resource("/org/tabos/banking/resources/comdirect.png")
        elif self._account.association == 'dsgv':
            self._image.set_from_resource("/org/tabos/banking/resources/dsgv.svg")
        elif self._account.association == 'bdb':
            self._image.set_from_resource("/org/tabos/banking/resources/bdb.jpg")
        elif self._account.association == 'bvr':
            self._image.set_from_resource("/org/tabos/banking/resources/bvr.svg")

        # Set text
        self._product_label.set_markup(f'<b>{self._account.product_name}</b>')
        self._account_label.set_text(self._account.iban)
        self._owner_label.set_markup(f'<small>{self._account.owner_name}</small>')

        # Set balance
        balance = locale.currency(self._account.balance, grouping=True, symbol=False)
        self._balance_label.set_text(f'{balance} {self._account.currency}')

        if self._account.balance < 0.0:
            self._balance_label.set_name('debit')
        else:
            self._balance_label.set_name('credit')

        self.transactions = []
        for transaction in account.transactions:
            trow = TransactionRow(transaction, self._backend)
            self.transactions.append(trow)

    @property
    def account(self):
        return self._account

    @account.setter
    def account(self, account: AccountData) -> None:
        self.set_info(account)
