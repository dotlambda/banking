# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk

from banking.backend.backend import BankingBackend


@Gtk.Template(resource_path='/org/tabos/banking/ui/clients.ui')
class Clients(Gtk.Dialog):
    __gtype_name__ = 'Clients'

    _bank_entry = Gtk.Template.Child()
    _user_entry = Gtk.Template.Child()
    _password_entry = Gtk.Template.Child()
    _server_label = Gtk.Template.Child()
    _name_label = Gtk.Template.Child()
    _blz_label = Gtk.Template.Child()
    _bic_label = Gtk.Template.Child()
    _create_button = Gtk.Template.Child()
    _clients_group = Gtk.Template.Child()

    def __init__(self, backend: BankingBackend, **kwargs) -> None:
        super().__init__(**kwargs)

        self._backend = backend
        self._institute = None

        # self._clients_group.set_visible(len(self._backend.accounts) > 0)
        # for account in self._backend.accounts:
        #     row = Adw.ActionRow()
        #     row.set_title(account.bank_name)

        #     self._clients_group.add(row)

        self.connect('response', self._on_response)

    @Gtk.Template.Callback()
    def _on_search_changed(self, _) -> None:
        bank = self._bank_entry.get_text()

        if len(bank) > 0:
            self._institute = self._backend.find_institute(bank)
        else:
            self._institute = None

        if self._institute:
            self._name_label.set_text(self._institute['institute'])
            self._server_label.set_text(self._institute['url'])
            self._blz_label.set_text(str(self._institute['blz']))
            self._bic_label.set_text(str(self._institute['bic']))
            self._create_button.set_sensitive(True)
        else:
            self._name_label.set_text('')
            self._server_label.set_text('')
            self._blz_label.set_text('')
            self._bic_label.set_text('')
            self._create_button.set_sensitive(False)

    def _on_response(self, accounts, ret) -> None:
        if ret == Gtk.ResponseType.APPLY:
            self._backend.add_client(
                self._user_entry.get_text(),
                self._password_entry.get_text(),
                self._server_label.get_text(),
                str(self._institute['blz']) if self._institute else self._bank_entry.get_text())

            self._backend.refresh_accounts()

        self.destroy()
