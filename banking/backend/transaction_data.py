# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import datetime

from gi.repository import GObject


class TransactionData(GObject.GObject):
    def __init__(self, transaction):
        super().__init__()
        self._transaction = transaction

    @property
    def name(self):
        # Helper property to extract the best suitable name for the transaction
        if self.applicant_name:
            name = self.applicant_name
            if name.startswith('PayPal'):
                match = re.search('\\.*\\. (.+?),', self.purpose)
                if match:
                    name = match.group(1).strip()
        elif len(self.posting_text) > 0:
            name = self.posting_text
        else:
            name = self.purpose

        return name

    def _get_value(self, key):
        val = self._transaction.get(key)
        if val and val.strip():
            return val

        return ''

    @property
    def date(self) -> datetime:
        return datetime.datetime.strptime(self._get_value('date'), '%Y-%m-%d')

    @property
    def entry_date(self) -> datetime:
        return datetime.datetime.strptime(self._get_value('entry_date'), '%Y-%m-%d')

    @property
    def applicant_name(self) -> str:
        return self._get_value('applicant_name')

    @property
    def posting_text(self) -> str:
        return self._get_value('posting_text')

    @property
    def amount(self) -> float:
        return self._transaction['amount'] or 0.0

    @property
    def currency(self) -> str:
        return self._transaction['currency'] or ''

    @property
    def iban(self) -> str:
        return self._get_value('applicant_iban')

    @property
    def bic(self) -> str:
        return self._get_value('applicant_bin')

    @property
    def purpose(self) -> str:
        return self._get_value('purpose')

    @property
    def applicant_creditor_id(self) -> str:
        return self._get_value('applicant_creditor_id')

    @property
    def additional_position_reference(self) -> str:
        return self._get_value('additional_position_reference')

    @property
    def end_to_end_reference(self) -> str:
        return self._get_value('end_to_end_reference')
